package com.bbva.cognosfera.test

import java.util.Date

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.kafka010.ConsumerStrategies._
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies._
import org.apache.log4j.{Level, Logger}



/**
  * Created by jpfernandez on 15/11/16.
  */
object DirectKafka10KerbWordCount {



  def main(args: Array[String]) {


    if (args.length < 3) {
      System.err.println(s"""
                            |Usage: DirectKafkaWordCount <brokers> <topics>
                            |  <brokers> is a list of one or more Kafka brokers
                            |  <topics> is a list of one or more kafka topics to consume from
                            |  <path>  path to hdfs file to save the output
        """.stripMargin)
      System.exit(1)
    }



    val Array(brokers, topics, path) = args

    // Create context with 2 second batch interval
    val sparkConf = new SparkConf().setAppName("DirectKafka10WordCount")
    val ssc = new StreamingContext(sparkConf, Seconds(10))


    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> brokers,
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "example",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean),
      "security.protocol" -> "SASL_PLAINTEXT"
    )


    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String]( Array(topics), kafkaParams)
    )

    val record = stream.map(record => (record.key, record.value))

    val lines = record.map(_._2)
    val words = lines.flatMap(_.split(" "))
    val wordCounts = words.map(x => (x, 1)).reduceByKey(_ + _)

    // DON'T USE RDDs. THIS IS ONLY FOR TESTING A KERBERIZED KAFKA

    wordCounts.foreachRDD(rdd => {


      if(!rdd.isEmpty) {
        val date = new Date()

        val sdf1 = new java.text.SimpleDateFormat("MMdd")
        val sufix1 = sdf1.format(date)
        val sdf2 = new java.text.SimpleDateFormat("hhmmss")
        val sufix2 = sdf2.format(date)

        rdd.saveAsTextFile(s"$path/$sufix1/$sufix2")
      }

    })

    wordCounts.print()

    ssc.start()
    ssc.awaitTermination()

    }

}
