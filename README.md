# README #

This project shows how to consume from kerberized kafka with spark streaming, using the Consumer developed by Hortonworks

Check the following links:

https://docs.hortonworks.com/HDPDocuments/HDP2/HDP-2.5.0/bk_spark-component-guide/content/using-spark-streaming.html

http://lets-do-something-big.blogspot.com.es/2016/09/enterprise-kafka-and-spark-kerberos.html?view=sidebar

https://elephant.tech/spark-2-0-streaming-from-ssl-kafka-with-hdp-2-4/